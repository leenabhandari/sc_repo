## Smartcart - Mall Assistant


https://www.youtube.com/watch?v=gHlZvBZcuvE

### Introduction

###### SmartCart is a ubiquitous computing system aimed

###### at providing consumers of a shopping mall with a

###### better shopping experience using an Android

###### application that helps them to -

- voice/ text conversation
- navigate to the right aisles
- locate the desired clothing items
- get information about these items
- check out the products that they desire by paying

###### for them via online payment gateways.


### Proposed system

```
Voice guided AR
navigation to required
product aisles
```
```
Chatbot and stylist to
answer consumer queries
```
```
QR code scanning for
adding items to cart
```
```
Self checkout and
automated payments
```
```
Database management of all
product items in the store.
```

### Alignment

- The project **aligns** with the domain of **Ubiquitous and Pervasive computing(Technology)** in the

- sector of the **retail industry** in India.

- **The social aspect** - We also aim to provide **visually disabled** consumers with a better shopping

- experience by incorporating accessibility features such as indoor navigation and voice enabled
- chatbot which would interact and give details about the product.


- The overall **impact** would be improved experience of a customer as they will be able to locate sections


- by themselves in a mall, would have a virtual assistant to guide them and they won’t have to stand in
- queues for billing and everything would be done at a touch of a button and eventually, improve the
- sales of the store.

- **Existing systems** - Amazon Go eliminated billing counters. However, it uses a lot of AI algorithms


- and computer vision which is expensive and hard to be implemented in India. Sensors and cameras
- are required on a large scale.


### Approach

#### (Problem)

- Consider a traditional shopping system. If a consumer enters a
    store and whats to buy a dress for a meeting. The person has to
    first locate the formals section. In case of huge malls, this is
    tiresome as manual assistance is not always available. After
    picking the right product, the next trouble is at the billing
    counters. A lot of people avoid shopping at rush hours because
    more time is spent on billing than on shopping. The overall
    experience becomes tiresome and this eventually affects the
    sales of the store.
- The experience is even worse for visually impaired consumers
    as they may need a helping hand at every stage of the process.


### Approach

#### (Concept)

```
In our system, if a person wants to buy a dress, they simply specify
about it in application. Suppose a person says they need a dress for a
meeting, the app understands they want to by a formal dress and using
the indoor navigation takes the person to their destination. Once, the
consumer selects the product, they can scan the QR code on the product
and complete the billing through wallet or card payment option.
```
```
To make this system suitable for the visually impaired, all the
instructions, be it navigation or giving out the product details, would be
voice based.
```

### Approach

#### (Scope and Limitations)

```
Our system will be suitable for any large scale shopping centres.

```
- Further, using **business intelligence** and a person’s shopping
    history, likes, etc. _personalised offers_ could be made to improve
    the sales. The virtual assistant can suggest more sections to
    check out while in store based on the location.
- For ensuring security, the _RFID tag removers_ would be
    _programmed_ to remove tags only on payment completion.
- Other methodologies like VPS, wifi-router strength would be
    used for indoor navigation.

```
The limitations include-
```
1. Internet connection and smartphone for its working.
2. Proper security for the system.
3. Social acceptance and proper implementation to the new system.


### Resources Required

**- Prerequisites-**
    Android based device, Internet connectivity
**- Development and implementation-**
    Stickers for Augmented-Reality based indoor navigation
    Backend support- Firebase
    Chatbot support- IBM Watson
    Development device having installed softwares.


### Technology
### Stack
![](https://i.ibb.co/LnDJ9k9/Screenshot-22.png)

**Business Canvas ModelBusiness Model** (^)
**Key
Activities**
● Fetching and
modeling the
stores’ product
catalog database
● Adding QR
stickers onto the
products and
AR markers
onto the aisles
● Setting up
back-end
connectivity and
the EAS system
**Key
Partners**
● EAS system
manufacturers.
● QR and label tag
providers.
● Store owners
**Customers**
● Shopping mall
and store
owners.
● Ad companies
● Better
product-information
accessibility
● Hassle free cashless
payment system
● Added profits by
reducing store
assistants and
checkout counters
● Possibility of data
analytics and
business
intelligence to
maximize revenues
**Customer
Relationship**
● Assistance for
system set-up
● Timely
Maintenance and
scaling
● Database
administration
● Customer care
helpline
**Channel**
● Digital
marketing and
social media.
● Campaigning
& advertising
at Malls
**Key
resources**
● Product
catalogs
● In-house
servers for
back-end
hosting
● Electronic
Article
Surveillance
System

###### Value Proposition


### Conclusion

```
The use of ‘Smartcart - Mall Assistant’ will completely
revolutionize the shopping experience of the customers. The proposed
application addresses a number of problems faced by the customers
such as not knowing the exact location of the product, not knowing the
details of the product which the consumer wishes to buy and standing
in long queues for billing.

The usage of Smartcart will save a considerable amount of time
and effort of the customer, thus attracting more number of customers
towards shopping malls. Besides the application is not only designed
for the general public but also for the visually impaired people.
Smartcart is beneficial for the mall owners as automation will ensure
efficiency , reduction in the number of employees and increment in the sales of the mall.
```
 

### Created By-
- Leena Bhandari
- Aaishwarya Darandale
- Ansh Puri
- Aditya Joshi

# Thank You!


