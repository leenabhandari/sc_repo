package com.example.aishu.login;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeleteT extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/delete.php";
    DataModel indexdel;
    String tid;
    Context context;
    ArrayList<DataModel> dataModel;
    ListView listView;
    CustomAdapter adapter;
    String price;
    int pprice;
    TextView txtprice;

    public DeleteT(String tid, DataModel indexdel, Context context, ArrayList<DataModel> dataModel, ListView listView, CustomAdapter adapter, TextView txtprice){
        this.tid=tid;
        this.indexdel=indexdel;
        this.context=context;
        this.dataModel=dataModel;
        this.listView=listView;
        this.adapter=adapter;
        this.txtprice=txtprice;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("success")) {
                    //Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
                    dataModel.remove(indexdel);
//                    String strprice=txtprice.getText().toString();
//                    price=Integer.getInteger(strprice);
//                    price=price-Integer.getInteger(indexdel.getPrice());
//                    txtprice.setText(price);
                    price=txtprice.getText().toString();
                    pprice=Integer.parseInt(price)-Integer.parseInt(indexdel.getPrice());
                    txtprice.setText(pprice+"");
                    adapter = new CustomAdapter(dataModel, context);
                    listView.setAdapter(adapter);
                    Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",tid);

                return params;
            }
        };
        mRequestQueue.add(postrequest);

        return null;
    }
}
