package com.example.aishu.login;

import android.view.View;
import android.widget.ListView;

public class DataModel2 {


    String pid;
    String iname;
    String price;
    String imgurl;
    String itime;

    public String getItime() {
        return itime;
    }

    public String getPid() {
        return pid;
    }
    public String getIname() {
        return iname;
    }

    public String getPrice() {
        return price;
    }

    public String getImgurl() {
        return imgurl;
    }





    public DataModel2(String pid, String iname, String price, String imgurl, String itime)
    {
        this.pid=pid;
        this.iname=iname;
        this.price=price;
        this.imgurl=imgurl;
        this.itime=itime;
    }
}
