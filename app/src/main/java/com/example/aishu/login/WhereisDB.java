package com.example.aishu.login;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WhereisDB extends AsyncTask {
    String item;
    Context context;

    public String getLocation() {
        return location;
    }

    String location;

    WhereisDB(String item, Context context)
    {
        this.item=item;
        this.context=context;
    }

    private static final String baseurl="https://smartcart2.000webhostapp.com/in_location.php";
    @Override
    protected Object doInBackground(Object[] objects) {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                {
                    // Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                  location=response;
                    ((ChatActivity)context).loc_print(location);
//                    ChatActivity o=new ChatActivity();
//                    o.loc_print(location);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("item",item);
                return params;
            }
        };
        mRequestQueue.add(postrequest);

        return null;
    }
}
